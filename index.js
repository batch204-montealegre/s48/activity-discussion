// let posts = [];
// let count = 1;

fetch('https://jsonplaceholder.typicode.com/posts').then((response)=> response.json()).then(data =>{
	showPosts(data)
	
})
let addForm = document.querySelector('#form-add-post');

addForm.addEventListener("submit", (e) => {
	e.preventDefault()

	// console.log("Hello")
	posts.push({
		id: count,
		title : document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	})
	count ++
	console.log(posts);
	showPosts(posts);
})

const showPosts = (posts) => {
	let postEntries = '';

	posts.forEach((post) => {
		postEntries += `
			<div id ="post-${post.id}">
			<h3 id = "post-title-${post.id}">${post.title}</h3>
			<p id = "post-body-${post.id}">${post.body}</p>
			<button onClick = "editPost(${post.id})">Edit</button>
			<button onclick = "deletePost(${post.id})">Delete</button>
			</div>
			`
			// console.log(post.id)
	})



	document.querySelector('#div-post-entries').innerHTML = postEntries;
}
const deletePost = (id) =>{
	
 document.querySelector(`#post-${id}`).remove()
	

}



const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML
	let body = document.querySelector(`#post-body-${id}`).innerHTML

	document.querySelector(`#txt-edit-id`).value = id;
	document.querySelector(`#txt-edit-title`).value = title;
	document.querySelector(`#txt-edit-body`).value = body;
	}

	document.querySelector('#form-edit-post').addEventListener('submit', (e) => {e.preventDefault() 
	for(let i = 0; i<posts.length; i++){ 
	if(document.querySelector('#txt-edit-id').value===posts[i].id.toString()){

		posts[i].title=document.querySelector("#txt-edit-title").value;
		posts[i].body=document.querySelector("#txt-edit-body").value;

		showPosts(posts)
		console.log(posts)

		break;
	}
}
})

